import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListWithdrawalsComponent } from './list-withdrawals.component';

describe('ListWithdrawalsComponent', () => {
  let component: ListWithdrawalsComponent;
  let fixture: ComponentFixture<ListWithdrawalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListWithdrawalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListWithdrawalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
