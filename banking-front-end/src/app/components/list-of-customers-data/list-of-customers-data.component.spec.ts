import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfCustomersDataComponent } from './list-of-customers-data.component';

describe('ListOfCustomersDataComponent', () => {
  let component: ListOfCustomersDataComponent;
  let fixture: ComponentFixture<ListOfCustomersDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfCustomersDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfCustomersDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
