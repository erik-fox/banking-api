import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDepositsComponent } from './list-deposits.component';

describe('ListDepositsComponent', () => {
  let component: ListDepositsComponent;
  let fixture: ComponentFixture<ListDepositsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDepositsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDepositsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
