import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AddWithdrawalComponent } from './components/add-withdrawal/add-withdrawal.component';
import { LoginComponent } from './components/login/login.component';
import { AddAccountsComponent } from './components/add-accounts/add-accounts.component';
import { AddCustomerComponent } from './components/add-customer/add-customer.component';
import { AddDepositComponent } from './components/add-deposit/add-deposit.component';
import { ListOfCustomersDataComponent } from './components/list-of-customers-data/list-of-customers-data.component';
import { ListBillsComponent } from './components/list-bills/list-bills.component';
import { ListDepositsComponent } from './components/list-deposits/list-deposits.component';
import { ListWithdrawalsComponent } from './components/list-withdrawals/list-withdrawals.component';

@NgModule({
  declarations: [
    AppComponent,
    AddWithdrawalComponent,
    LoginComponent,
    AddAccountsComponent,
    AddCustomerComponent,
    AddDepositComponent,
    ListOfCustomersDataComponent,
    ListBillsComponent,
    ListDepositsComponent,
    ListWithdrawalsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
