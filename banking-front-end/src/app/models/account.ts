import { Customer } from './customer';

export class Account {
    
    private nickname: string;
    private rewards: number;
    private balance: number;
    private customer: Customer;

}
