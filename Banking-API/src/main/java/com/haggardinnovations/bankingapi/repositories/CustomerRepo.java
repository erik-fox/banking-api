package com.haggardinnovations.bankingapi.repositories;

import com.haggardinnovations.bankingapi.domains.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@Repository
@CrossOrigin(origins = "http://localhost:4200")
public interface CustomerRepo extends CrudRepository<Customer, Long> {



}

