package com.haggardinnovations.bankingapi.repositories;


import com.haggardinnovations.bankingapi.domains.Deposit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository
@CrossOrigin(origins = "http://localhost:4200")
public interface DepositRepo extends CrudRepository<Deposit, Long> {


}
